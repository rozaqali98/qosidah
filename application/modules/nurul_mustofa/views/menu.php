<!DOCTYPE html>
<html lang="en-US">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Medigo Blue, free responsive template</title>
    <meta name="keywords" content="">
	<meta name="description" content="">
    <meta name="author" content="templatemo">
    <!-- 
	Medigo Template
	http://www.templatemo.com/preview/templatemo_460_medigo
    -->
    
	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700itali" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,500,200,100,600" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/css/misc.css">
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/css/blue-scheme.css">
	
	<!-- JavaScripts -->
	<script src="<?php base_url(); ?>/qosidah/vendor/js/jquery-1.10.2.min.js"></script>
	<script src="<?php base_url(); ?>/qosidah/vendor/js/jquery-migrate-1.2.1.min.js"></script>

	<link rel="shortcut icon" href="<?php base_url(); ?>/qosidah/vendor/images/favicon.ico" type="image/x-icon" />

</head>
<body>
	<div class="responsive_menu">
        <ul class="main_menu">
            <li><a href="index.html">Home</a></li>
            <li><a href="#">Portfolio</a>
            	<ul>
            		
            		<?php foreach($data as $u) {?>
            		<li><a href="portfolio.html"><?php echo $u->name;  }?></a></li>
            		<!-- <li><a href="project-image.html">Majlis Rasulullah SAW</a></li>
            		<li><a href="project-slideshow.html">Majlis Warotsatul Musthofa</a></li>
            		<li><a href="project-slideshow.html">Majlis Ahbabul Musthofa</a></li>
            		<li><a href="project-slideshow.html">Majlis Syubbanul Muslimin</a></li> -->
            	</ul>
            </li>
            <li><a href="#">Blog</a>
				<ul>
					<li><a href="blog.html">Blog Standard</a></li>
					<li><a href="blog-single.html">Blog Single</a></li>
                    <li><a href="#">visit templatemo</a></li>
				</ul>
            </li>
            <li><a href="archives.html">Archives</a></li>
            <li><a href="contact.html">Contact</a></li>
        </ul> <!-- /.main_menu -->
    </div> <!-- /.responsive_menu -->

	<header class="site-header clearfix">
		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="pull-left logo">
						<a href="index.html">
							<!-- <img src="<?php base_url(); ?>/qosidah/vendor/images/logo.png" alt="Medigo by templatemo"> -->
						</a>
					</div>	<!-- /.logo -->

					<div class="main-navigation pull-right">

						<nav class="main-nav visible-md visible-lg">
							<ul class="sf-menu">
								<li class="active"><a href="<?php echo site_url('qosidah') ?>">Home</a></li>
					            <li><a href="#">Lirik Majlis</a>
					            	<ul>

					            		<?php foreach($data as $u) {?>
            							<li><a href="<?php echo site_url($u->url_menu.'/'.$u->id); ?>"><?php echo $u->name;  }?></a></li>
					            		
					            		<!-- <li><a href="<?php echo site_url('qosidah/nurul') ?>">Majlis Nurul Musthofa</a></li>
					            		<li><a href="project-image.html">Majlis Rasulullah SAW</a></li>
					            		<li><a href="project-slideshow.html">Majlis Warotsatul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Ahbabul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Syubbanul Muslimin</a></li> -->
         					            	</ul>
					            </li>
					            <li><a href="#">Blog</a>
									<ul>
										<li><a href="blog.html">Blog Standard</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                        <li><a href="#">visit templatemo</a></li>
									</ul>
					            </li>
					            <li><a href="archives.html">Archives</a></li>
					            <li><a href="contact.html">Contact</a></li>
							</ul> <!-- /.sf-menu -->
						</nav> <!-- /.main-nav -->

						<!-- This one in here is responsive menu for tablet and mobiles -->
					    <div class="responsive-navigation visible-sm visible-xs">
					        <a href="#nogo" class="menu-toggle-btn">
					            <i class="fa fa-bars"></i>
					        </a>
					    </div> <!-- /responsive_navigation -->

					</div> <!-- /.main-navigation -->

				</div> <!-- /.col-md-12 -->

			</div> <!-- /.row -->

		</div> <!-- /.container -->
	</header> <!-- /.site-header -->

	<section id="homeIntro" class="parallax first-widget">
	    <div class="parallax-overlay">
		    <div class="container home-intro-content">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h2>Lirik Qosidah Majlis</h2>
		        		<p>Mengharapkan syafa'at Rasulullah SAW dengan perantara bersholawat kepada Rasulullah SAW</p>
		        		
		        	</div> <!-- /.col-md-12 -->
		        </div> <!-- /.row -->
		    </div> <!-- /.container -->
	    </div> <!-- /.parallax-overlay -->
	</section> <!-- /#homeIntro -->



	<section class="dark-content portfolio">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-header">
						<h2 class="section-title">Majelis</h2>
						<p class="section-desc">Macam-macam qosidah majelis yang tertampil pada website ini</p>
					</div> <!-- /.section-header -->
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container -->
		
		<div id="portfolio-carousel" class="portfolio-carousel portfolio-holder">
			<div class="item">
				<div class="thumb-post">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="portfolio-infos">
								<span class="meta-category">Habib Hasan bin Ja'far Assegaf</span>
								<h3 class="portfolio-title"><a href="project-slideshow.html">Majelis Nurul Musthofa</a></h3>
							</div>
							<div class="portfolio-expand">
								<a class="fancybox" href="<?php base_url(); ?>/qosidah/vendor/images/includes/nurulmusthofa.jpg" title="Majelis Nurul Musthofa">
									<i class="fa fa-expand"></i>
								</a>
							</div>
						</div>
					</div>
					<img src="<?php base_url(); ?>/qosidah/vendor/images/includes/nurulmusthofa.jpg" alt="Majelis Nurul Musthofa">
				</div>
			</div> <!-- /.item -->
			<div class="item">
				<div class="thumb-post">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="portfolio-infos">
								<span class="meta-category">Habib Mundzir al musawa</span>
								<h3 class="portfolio-title"><a href="project-slideshow.html">Majelis Rasulullah SAW</a></h3>
							</div>
							<div class="portfolio-expand">
								<a class="fancybox" href="<?php base_url(); ?>/qosidah/vendor/images/includes/majlelisrasulullah.jpg" title="Majelis Rasulullah SAW">
									<i class="fa fa-expand"></i>
								</a>
							</div>
						</div>
					</div>
					<img src="<?php base_url(); ?>/qosidah/vendor/images/includes/majlelisrasulullah.jpg" alt="Majelis Rasulullah SAW">
				</div>
			</div> <!-- /.item -->
			<div class="item">
				<div class="thumb-post">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="portfolio-infos">
								<span class="meta-category">Habib Muhammad Al-Bagir bin Alwi Bin Yahya</span>
								<h3 class="portfolio-title"><a href="project-slideshow.html">Majelis Warotsatul Musthofa</a></h3>
							</div>
							<div class="portfolio-expand">
								<a class="fancybox" href="images/includes/majliswarotsatulmusthofa.jpg" title="Majelis Warotsatul Musthofa">
									<i class="fa fa-expand"></i>
								</a>
							</div>
						</div>
					</div>
					<img src="<?php base_url(); ?>/qosidah/vendor/images/includes/majliswarotsatulmusthofa.jpg" alt="Majelis Warotsatul Musthofa">
				</div>
			</div> <!-- /.item -->
			<div class="item">
				<div class="thumb-post">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="portfolio-infos">
								<span class="meta-category">Habib Syech bin Abdul Qodir Assegaf</span>
								<h3 class="portfolio-title"><a href="project-slideshow.html">Majelis Ahbabul Musthofa</a></h3>
							</div>
							<div class="portfolio-expand">
								<a class="fancybox" href="images/includes/habibsyech.jpg" title="Majelis Ahbabul Musthofa">
									<i class="fa fa-expand"></i>
								</a>
							</div>
						</div>
					</div>
					<img src="<?php base_url(); ?>/qosidah/vendor/images/includes/habibsyech.jpg" alt="Majelis Ahbabul Musthofa">
				</div>
			</div> <!-- /.item -->
			<div class="item">
				<div class="thumb-post">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="portfolio-infos">
								<span class="meta-category">Gus Hafidz</span>
								<h3 class="portfolio-title"><a href="project-slideshow.html">Majelis Syubbanul Muslimin</a></h3>
							</div>
							<div class="portfolio-expand">
								<a class="fancybox" href="images/includes/gus.jpg" title="Majelis Syubbanul Muslimin">
									<i class="fa fa-expand"></i>
								</a>
							</div>
						</div>
					</div>
					<img src="<?php base_url(); ?>/qosidah/vendor/images/includes/gus.jpg" alt="Majelis Syubbanul Muslimin">
				</div>
			</div> <!-- /.item -->
		
		</div> <!-- /#owl-demo -->
	</section> <!-- /.portfolio-holder -->

	

	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="footer-nav clearfix">
						<ul class="footer-menu">
							<li><a href="index.html">Home</a></li>
							<li><a href="portfolio.html">Portfolio</a></li>
							<li><a href="blog.html">Blog Posts</a></li>
							<li><a href="archives.html">Shortcodes</a></li>
							<li><a href="contact.html">Contact</a></li>
						</ul> <!-- /.footer-menu -->
					</nav> <!-- /.footer-nav -->
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
			<div class="row">
				<div class="col-md-12">
					<p class="copyright-text">Copyright &copy; 2020 Lirik Qosidah 
                    | Design: templatemo</p>
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</footer> <!-- /.site-footer -->

	<!-- Scripts -->
	<script src="<?php base_url(); ?>/qosidah/vendor/js/min/plugins.min.js"></script>
	<script src="<?php base_url(); ?>/qosidah/vendor/js/min/medigo-custom.min.js"></script>

</body>
</html>