	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="footer-nav clearfix">
						<ul class="footer-menu">
							<li><a href="index.html">Home</a></li>							
							<li><a href="<?php echo site_url('qosidah/contact'); ?>">Contact</a></li>
						</ul> <!-- /.footer-menu -->
					</nav> <!-- /.footer-nav -->
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
			<div class="row">
				<div class="col-md-12">
					<p class="copyright-text">Copyright &copy; 2020 Lirik Qosidah</p>
				</div> <!-- /.col-md-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</footer> <!-- /.site-footer -->

	<!-- Scripts -->
	<script src="<?php base_url(); ?>/qosidah/vendor/js/min/plugins.min.js"></script>
	<script src="<?php base_url(); ?>/qosidah/vendor/js/min/medigo-custom.min.js"></script>


</body>
</html>