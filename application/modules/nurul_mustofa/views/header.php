<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Lirik Qosidah</title>
    <meta name="keywords" content="">
	<meta name="description" content="">
    <meta name="author" content="templatemo">
    <!-- 
	Medigo Template
	http://www.templatemo.com/preview/templatemo_460_medigo
    -->

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700itali" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,500,200,100,600" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/css/misc.css">
	<link rel="stylesheet" href="<?php base_url(); ?>/qosidah/vendor/css/blue-scheme.css">
	
	<!-- JavaScripts -->
	<script src="<?php base_url(); ?>/qosidah/vendor/js/jquery-1.10.2.min.js"></script>
	<script src="<?php base_url(); ?>/qosidah/vendor/js/jquery-migrate-1.2.1.min.js"></script>

	<!-- <link rel="shortcut icon" href="<?php base_url(); ?>/qosidah/vendor/images/favicon.ico" type="image/x-icon" /> -->

</head>
<body>


	<div class="responsive_menu">
        <ul class="main_menu">

            <li class="active"><a href="<?php echo site_url('qosidah') ?>">Home</a></li>
					            	<li><a href="#">Lirik Majlis</a>
					            	<ul>					            		 
					            		<?php foreach ($tes as $u) { ?>					            			
					            		
					            		<li><a href="<?php echo site_url($u->url_menu) ?>"><?php echo $u->name; }?></a></li>

					            		<!-- <li><a href="<?php echo site_url('qosidah/nurul') ?>">Majlis Nurul Musthofa</a></li>
					            		<li><a href="project-image.html">Majlis Rasulullah SAW</a></li>
					            		<li><a href="project-slideshow.html">Majlis Warotsatul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Ahbabul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Syubbanul Muslimin</a></li> -->
         					            	</ul>
					            	</li>
           
            <li><a href="<?php echo site_url('qosidah/contact'); ?>">Contact</a></li>
        </ul> <!-- /.main_menu -->
    </div> <!-- /.responsive_menu -->

	<header class="site-header clearfix">
		<div class="container">
			<div class="row">

				<div class="col-md-12">

					<div class="pull-left logo">
						<a href="index.html">
							<img src="<?php base_url(); ?>/qosidah/vendor/images/logo.png" alt="Medigo">
						</a>
					</div>	<!-- /.logo -->

					<div class="main-navigation pull-right">

						<nav class="main-nav visible-md visible-lg">
							<ul class="sf-menu">
								<li class="active"><a href="<?php echo site_url('qosidah') ?>">Home</a></li>
					            	<li><a href="#">Lirik Majlis</a>
					            	<ul>					            		 
					            		<?php foreach ($tes as $u) { ?>					            			
					            		
					            		<li><a href="<?php echo site_url($u->url_menu) ?>"><?php echo $u->name; }?></a></li>

					            		<!-- <li><a href="<?php echo site_url('qosidah/nurul') ?>">Majlis Nurul Musthofa</a></li>
					            		<li><a href="project-image.html">Majlis Rasulullah SAW</a></li>
					            		<li><a href="project-slideshow.html">Majlis Warotsatul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Ahbabul Musthofa</a></li>
					            		<li><a href="project-slideshow.html">Majlis Syubbanul Muslimin</a></li> -->
         					            	</ul>
					            	</li>
					            	
					            	<li><a href="contact.html">Contact</a></li>
					            </li>
							</ul> <!-- /.sf-menu -->
						</nav> <!-- /.main-nav -->

						<!-- This one in here is responsive menu for tablet and mobiles -->
					    <div class="responsive-navigation visible-sm visible-xs">
					        <a href="#nogo" class="menu-toggle-btn">
					            <i class="fa fa-bars"></i>
					        </a>
					    </div> <!-- /responsive_navigation -->

					</div> <!-- /.main-navigation -->

				</div> <!-- /.col-md-12 -->

			</div> <!-- /.row -->

		</div> <!-- /.container -->
	</header> <!-- /.site-header -->
